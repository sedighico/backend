<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    
        // $this->call(UsersTableSeeder::class);
      $this->call(commentsettingseed::class);
      $this->call(contactus::class);
      $this->call(defaultsetting::class);
      $this->call(featuremodel::class);
      $this->call(invoive_post_default::class);
      $this->call(Joblist::class);
    }
}
