<?php

namespace App\Http\Controllers\User\Product;

use App\Http\Controllers\Controller;
use App\Model\Attr\attr_product_option;
use App\Model\Product\p_attr_option_value;
use App\Model\Product\p_attr_value;
use App\Model\Product\p_price;
use Illuminate\Http\Request;

class ProdcutOptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,p_attr_option_value $attr_option_value,p_price $p_price,p_attr_value $attr_value)
    {
    $parent=$attr_value->where('id',$request->attr)->first();
    $product=$parent->product;
    if($attr_option_value->where('parent',$request->id)->where('attr',$request->attr)->count()==0){
     $my=$attr_option_value->updateOrCreate(
            ['parent'=>$request->id,'attr'=>$request->attr],
            ['value'=>0 ]
        );
     $p_price->updateOrCreate(
         ['parent'=>$product,'attr'=>$my->id],
         ['price'=>0,'discount'=>0,'percent'=>0]
     );
    }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,p_attr_option_value $attr_option_value)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,p_attr_option_value $attr_option_value,p_price $p_price)
    {
        $p_price->where('attr',$id)->delete();
        $attr_option_value->where('id',$id)->delete();
    }
}
