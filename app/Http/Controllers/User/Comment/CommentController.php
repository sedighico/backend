<?php

namespace App\Http\Controllers\User\Comment;

use App\Http\Controllers\Controller;
use App\Model\Product\p_prodcut;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use Psy\Util\Json;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

       $product=@$request->id;
        $myarray=collect();
        $mykey=Redis::keys('comment:request:*');
        foreach ($mykey as $key){
          $key=str_replace('medical_database_','',$key);
            //medical_database_comment:request:firstpage
            $getitem=Redis::get('comment:request:firstpage');
           // return $key;
                $comment= collect([
                    'comment'=>json_decode(Redis::get($key)),
                ]);
                $myarray->push($comment);

        }
        return $myarray;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        Redis::set('commentactive:'.$request->user['id'].':'.$request->product['id'],1);
        $comment                    =array();
        $comment['subject']         =$request->comment['subject'];
        $comment['text']            =$request->comment['text'];
        $comment['active']          =1;
        Redis::set('comment:'.$request->user['id'].':'.$request->product['id'],Json::encode($comment));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        Redis::del('comment:'.$id.':'.$request->product);
        Redis::del('subject1:'.$id.':'.$request->product);
        Redis::del('subject2:'.$id.':'.$request->product);
        Redis::del('subject3:'.$id.':'.$request->product);
        Redis::del('subject4:'.$id.':'.$request->product);
        Redis::del('commentactive:'.$id.':'.$request->product);

    }
}
