<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProdcutThumpnailResource;
use App\Http\Resources\Product\ProductViewResource;
use App\Model\Product\p_prodcut;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use phpDocumentor\Reflection\Types\Array_;

class productfavscontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(p_prodcut $p_prodcut)
    {
         $data= Redis::keys('fav:'.Auth::id().':*');

         $array=[];
         foreach ($data as $key){
                 $key=str_replace('laravel_database_','',$key);
                array_push($array,Redis::get($key));

         }
        $list=$p_prodcut->whereIn('id',$array)
            ->orderByRaw('FIELD(status,1, 2, 0)')
            ->with('toColor.toColor','toAttr');

        $list=$list->get();
        return   ProdcutThumpnailResource::collection($list);


       // return json_encode($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

      return  count(Redis::keys('fav:'.Auth::id().':'.$id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Redis::set('fav:'.Auth::id().':'.$id, $id);


        return  count(Redis::keys('fav:'.Auth::id().':'.$id));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Redis::del('fav:'.Auth::id().':'.$id);
        return  count(Redis::keys('fav:'.Auth::id().':'.$id));

    }
}
