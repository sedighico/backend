<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Mail\RequestMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redis;
use Psy\Util\Json;

class SetCommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return $request->all();

      $request->validate([
            'email' => 'required|email',
            'lastname' => 'required',
            'name' => 'required',
            'phone' => 'required',
            'subject' => 'required',
            'text' => 'required',
        ]);

        $comment                    =array();

        $comment['email']          =$request->email;
        $comment['lastname']             =$request->lastname;
        $comment['name']             =$request->name;
        $comment['phone']             =$request->phone;
        $comment['subject']             =$request->subject;
        $comment['text']             =$request->text;
        $comment['active']           =false;

        Redis::set('comment:request:'.$request->page,Json::encode($comment));
        return Mail::to($comment['email'])->send(new RequestMail());

        return  true;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

       if( count(Redis::keys('comment:'.Auth::id().':'.$id))==0){
           $comment= collect([
               'count'=>0,

           ]);
       }else{
           $comment= collect([
               'count'=>1,
               'comment'=>Redis::get('comment:'.Auth::id().':'.$id),
               'subject1'=>Redis::get('subject1:'.Auth::id().':'.$id),
               'subject2'=>Redis::get('subject2:'.Auth::id().':'.$id),
               'subject3'=>Redis::get('subject3:'.Auth::id().':'.$id),
               'subject4'=>Redis::get('subject4:'.Auth::id().':'.$id)

           ]);
       }

        return $comment;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {

    }
}
