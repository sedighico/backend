<?php

namespace App\Http\Controllers\View\Pages;

use App\Http\Controllers\Controller;
use App\Model\Blog\b_article;
use App\Model\Blog\b_group;
use App\Model\Gallery\g_group;
use App\Model\Pages\page_setting;
use App\Model\Product\p_group;
use App\Model\Sys\sys_setting;
use Illuminate\Http\Request;

class FirstpageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $gb=b_group::where('url',$request->Blog)->first();
        return collect([
            ['setting'=>sys_setting::where('lang',$request->lang)->first()],
            ['slider'=>g_group::where('urlname',$request->slider)->with('todetail')->first()],
            ['Blog'=>b_article::where('group',$gb->id)->limit(5)->orderBy('id','DESC')->get()],
            ['Product'=>p_group::where('lang',$request->lang)->with('toProduct.tomasterImage')->get()],
            ['about'=>b_article::where('url',$request->About)->first()],

        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
