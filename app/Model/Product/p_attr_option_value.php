<?php

namespace App\Model\Product;

use App\Model\Attr\attr_product;
use App\Model\Attr\attr_product_option;
use Illuminate\Database\Eloquent\Model;

class p_attr_option_value extends Model
{
    protected $fillable=['value','parent','attr'];
    function toPrice(){
        return $this->hasOne(p_price::class,'attr','id');
    }
    function toOption(){
        return $this->hasOne(attr_product_option::class,'id','parent');
    }
}
